package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.model.MovieDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;


@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);
    private final MovieDatabase movieDatabase = new MovieDatabase();

    @PostMapping("/users")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto movieDto) throws URISyntaxException {
        try {
            if (movieDto.getTitle() == null || movieDto.getYear() == null || movieDto.getImage() == null) {
                throw new Exception();
            }
            LOG.info("--- title: {}", movieDto.getTitle());
            LOG.info("--- year: {}", movieDto.getYear());
            LOG.info("--- image: {}", movieDto.getImage());
            LOG.info("==================================");
            movieDatabase.addMovie(movieDto);
        } catch (Exception e) {
            LOG.info("--- Wrong params");
            LOG.info("==================================");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.created(new URI("/api/users/")).build();
    }

    @GetMapping("/users")
    public ResponseEntity<List<MovieDto>> getMovies() {
        LOG.info("--- get all movies: {}", movieDatabase.getSize());
        LOG.info("==================================");
        return ResponseEntity.ok().body(movieDatabase.getMovieList());
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable Integer id, @RequestBody MovieDto movieDto) {
        LOG.info("--- id: {}", id);
        try {
            LOG.info("--- modified: {}", movieDatabase.getTitle(id));
            movieDatabase.updateMovie(id, movieDto);
        } catch (Exception e) {
            LOG.info("--- No movie with such id: {}", id);
            LOG.info("==================================");
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> getMovie(@PathVariable("id") Integer id) {
        LOG.info("--- id: {}", id);
        try {
            LOG.info("--- removed: {}", movieDatabase.getTitle(id));
            LOG.info("==================================");
            movieDatabase.removeMovie(id);
        } catch (Exception e) {
            LOG.info("--- No movie with such id: {}", id);
            LOG.info("==================================");
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
